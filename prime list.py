def is_prime(n):
  if n<2:
    return False
  for i in range(2,int(n**0.5)+1):
    if n%i==0:
      return False
  return True
def get_prime_numbers(numbers):
  prime_numbers=[]
  for num in numbers:
    if is_prime(num):
      prime_numbers.append(num)
  return prime_numbers

numbers=[2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17]
prime_numbers = get_prime_numbers(numbers)
print("prime numbers:",prime_numbers)
